package stevens.software.notetaking.util;

import com.google.gson.Gson;
import java.util.Date;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.model.Note;

public class TestUtils {

  public static Note getNoteModel() {
    Date date = new Date();

    Note note = new Note();
    note.setNoteId(1);
    note.setTitle("Note Title");
    note.setNote("Some Note");
//    note.setTimestamp(new Timestamp(date.getTime()));

    return note;
  }

  public static NoteDto getNoteDto() {

    Date date = new Date();

    NoteDto note = new NoteDto();
    note.setNoteId(1);
    note.setTitle("Note Title");
    note.setNote("Some Note");
//    note.setTimestamp(new Timestamp(date.getTime()));

    return note;
  }

  public static String convertToJsonString(Object object) {
    Gson gson = new Gson();
    return gson.toJson(object);
  }
}
