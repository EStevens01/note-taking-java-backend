package stevens.software.notetaking.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.mapper.NoteMapper;
import stevens.software.notetaking.repository.NoteTakingRepository;
import stevens.software.notetaking.util.TestUtils;

@ExtendWith(MockitoExtension.class)
public class NoteTakingServiceTest {

  @Mock
  private NoteTakingRepository noteTakingRepository;

  @Spy
  private NoteMapper mapper;

  @InjectMocks
  private NoteTakingService noteTakingService;


  @Test
  public void testGetAllNotes() {
    List<NoteDto> expected = Collections.singletonList(TestUtils.getNoteDto());
    when(noteTakingRepository.findAll())
        .thenReturn(Collections.singletonList(TestUtils.getNoteModel()));
    List<NoteDto> actual = noteTakingService.getAllNotes();
    assertEquals(expected, actual);
  }

  @Test
  public void testGetNoteById() {
    NoteDto expected = TestUtils.getNoteDto();
    when(noteTakingRepository.findById(1))
        .thenReturn(Optional.of(TestUtils.getNoteModel()));
    NoteDto actual = noteTakingService.getNoteById(1);
    assertEquals(expected, actual);
  }

  @Test
  public void testSaveNote() {
    NoteDto expected = TestUtils.getNoteDto();
    when(noteTakingRepository.save(TestUtils.getNoteModel())).thenReturn(TestUtils.getNoteModel());
    NoteDto actual = noteTakingService.saveNote(TestUtils.getNoteDto());
    assertEquals(expected, actual);
  }

  @Test
  public void testDeleteNote() {
    doNothing().when(noteTakingRepository).deleteById(1);
    noteTakingService.deleteNote(1);
    verify(noteTakingRepository, times(1)).deleteById(1);
  }

}
