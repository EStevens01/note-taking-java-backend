package stevens.software.notetaking.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static stevens.software.notetaking.util.TestUtils.convertToJsonString;
import static stevens.software.notetaking.util.TestUtils.getNoteDto;


import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.repository.NoteTakingRepository;
import stevens.software.notetaking.service.NoteTakingService;
import stevens.software.notetaking.util.TestUtils;

@SpringBootTest
@AutoConfigureMockMvc
public class NoteTakingControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private NoteTakingService noteTakingService;

  @MockBean
  private NoteTakingRepository noteTakingRepository;

  @Test
  public void testGetNotes() throws Exception {
    List<NoteDto> expected = Collections.singletonList(TestUtils.getNoteDto());
    when(noteTakingService.getAllNotes()).thenReturn(expected);
    mvc.perform(get("/notes")).andDo(print()).andExpect(status().isOk())
        .andExpect(content().json(convertToJsonString(expected)));
  }

  @Test
  public void testGetNoteById() throws Exception {
    NoteDto expected = TestUtils.getNoteDto();
    when(noteTakingService.getNoteById(1)).thenReturn(expected);
    mvc.perform(get("/note/1")).andDo(print()).andExpect(status().isOk())
        .andExpect(content().json(convertToJsonString(expected)));
  }

  @Test
  public void testSaveNote() throws Exception {
    NoteDto expected = getNoteDto();
    when(noteTakingService.saveNote(getNoteDto())).thenReturn(getNoteDto());
    mvc.perform(post("/note").contentType(MediaType.APPLICATION_JSON)
        .content(convertToJsonString(expected))).andDo(print())
        .andExpect(status().isCreated())
        .andExpect(content().json(convertToJsonString(expected)));
  }

  @Test
  public void testDeleteNote() throws Exception {
    doNothing().when(noteTakingRepository).deleteById(1);
    mvc.perform(delete("/note/1")).andExpect(status().isOk());
    verify(noteTakingService).deleteNote(1);
  }
}

