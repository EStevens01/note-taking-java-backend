package stevens.software.notetaking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import stevens.software.notetaking.model.Note;

public interface NoteTakingRepository extends JpaRepository<Note, Integer> {

}
