package stevens.software.notetaking.dto;

import lombok.Data;

@Data
public class NoteDto {

  private Integer noteId;
  private String title;
  private String note;
//  private Timestamp timestamp;

}
