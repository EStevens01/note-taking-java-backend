package stevens.software.notetaking.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.service.NoteTakingService;

//@Validated
@Controller
public class NoteTakingController {

  @Autowired
  private NoteTakingService noteTakingService;

  @GetMapping("/notes")
  public ResponseEntity<List<NoteDto>> getNotes() {
    return ResponseEntity.ok().body(noteTakingService.getAllNotes());
  }

  @CrossOrigin
  @GetMapping("/note/{id}")
  public ResponseEntity<NoteDto> getNoteById(@PathVariable Integer id) {
    return ResponseEntity.ok().body(noteTakingService.getNoteById(id));
  }

  @PostMapping("/note")
  public ResponseEntity<NoteDto> saveNote(@RequestBody NoteDto note) {
    return new ResponseEntity(noteTakingService.saveNote(note), HttpStatus.CREATED);
  }

  @DeleteMapping("/note/{id}")
  public ResponseEntity deleteNote(@PathVariable Integer id) {
    noteTakingService.deleteNote(id);
    return new ResponseEntity(HttpStatus.OK);
  }
}
