package stevens.software.notetaking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "notes")
public class Note {

  @Id
  @GeneratedValue
  private Integer noteId;

  @Column(name = "title")
  private String title;

  @Column(name = "note")
  private String note;

//  @Column(name = "timestamp")
//  private Timestamp timestamp;
}
