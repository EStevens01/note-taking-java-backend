package stevens.software.notetaking.mapper;

import org.springframework.stereotype.Service;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.model.Note;

@Service
public class NoteMapper {

  public Note toModel(NoteDto noteDto) {
    Note noteModel = new Note();
    noteModel.setNoteId(noteDto.getNoteId());
    noteModel.setTitle(noteDto.getTitle());
    noteModel.setNote(noteDto.getNote());

    return noteModel;
  }

  public NoteDto toDto(Note note) {
    NoteDto noteDto = new NoteDto();
    noteDto.setNoteId(note.getNoteId());
    noteDto.setTitle(note.getTitle());
    noteDto.setNote(note.getNote());

    return noteDto;
  }
}
