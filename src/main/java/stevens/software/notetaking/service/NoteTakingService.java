package stevens.software.notetaking.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stevens.software.notetaking.dto.NoteDto;
import stevens.software.notetaking.mapper.NoteMapper;
import stevens.software.notetaking.model.Note;
import stevens.software.notetaking.repository.NoteTakingRepository;

@Service
public class NoteTakingService {

  @Autowired
  private NoteTakingRepository noteTakingRepository;

  @Autowired
  private NoteMapper noteMapper;

  public List<NoteDto> getAllNotes() {
    return noteTakingRepository.findAll().stream().map(noteMapper::toDto)
        .collect(Collectors.toList());
  }

  public NoteDto getNoteById(Integer noteId) {
    Optional<Note> note = noteTakingRepository.findById(noteId);
//    return note.map(value -> noteMapper.toDto(value)).orElse(null);
    if (note.isPresent()) {
      System.out.println("hey 1");
      return noteMapper.toDto(note.get());

    } else {
      System.out.println("hey 2");

      return null;
    }
  }

  public NoteDto saveNote(NoteDto note) {
    return noteMapper.toDto(noteTakingRepository.save(noteMapper.toModel(note)));
  }

  public void deleteNote(Integer id) {
    noteTakingRepository.deleteById(id);
  }
}
